/*eslint-env node, mocha */
/*global expect */
/*eslint no-console: 0*/
'use strict';

import createComponent from 'helpers/shallowRenderHelper';

import CakeForm from 'components/CakeForm';

describe('MainComponent', () => {
  let CakeFormComponent;

  beforeEach(() => {
    CakeFormComponent = createComponent(CakeForm);
    console.log(CakeFormComponent)
  });

  it('should have its component name as default className', () => {
    expect(CakeFormComponent.props.className).to.equal('formWrapper');
  });
});

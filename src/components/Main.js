require('normalize.css/normalize.css');
require('styles/App.scss');

import React, { Component } from 'react'
import Form from './CakeForm';

class AppComponent extends Component {
  render() {
    return (
      <div className="wrapper">
      	<div id="form-header">Cake Enquiry Form</div>
        <Form />
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;

require('normalize.css/normalize.css');
require('styles/App.scss');

import React, { Component } from 'react';
import CheckboxGroup from 'react-checkbox-group';
import {RadioGroup, Radio} from 'react-radio-group';

class CakeForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      cakeTypes: [],
      celebrationType: '',
      otherCelebrationType: '',
      about: '',
      errorMessage: '',
      formValid: false
    };

    this.nameChanged = this.nameChanged.bind(this);
    this.emailChanged = this.emailChanged.bind(this);
    this.cakeTypesChanged = this.cakeTypesChanged.bind(this);
    this.celebrationTypeChanged = this.celebrationTypeChanged.bind(this);
    this.otherCelebrationTypeChanged = this.otherCelebrationTypeChanged.bind(this);
    this.aboutChanged = this.aboutChanged.bind(this);
    this.validate = this.validate.bind(this);
    this.reset = this.reset.bind(this);
  }

  render() {
    return (
      <div className="formWrapper">
        <form>
          <label htmlFor="name" className="heading">Name:</label>
          <input type="text" id="name" value={this.state.name} onChange={this.nameChanged} />

          <label htmlFor="email" className="heading">Email:</label>
          <input type="text" id="email" value={this.state.email} onChange={this.emailChanged} />
          
          <CheckboxGroup name="cakeTypes" value={this.state.cakeTypes} onChange={this.cakeTypesChanged} >
            {
              Checkbox => (
                <div id="typeOfCakes">
                <label htmlFor="typeOfCakes" className="heading">Type of cake:</label>
                  <label>
                    <Checkbox value="cupcakes"/>Cupcakes
                  </label>
                  <label>
                    <Checkbox value="cheesecakes"/>Cheesecakes
                  </label>
                  <label>
                    <Checkbox value="buttercakes"/>Butter cakes
                  </label>
                  <label>
                    <Checkbox value="mudcakes"/>Mudcakes
                  </label>
                </div>
              )
            }
          </CheckboxGroup>

          <RadioGroup name="celebrationType" selectedValue={this.state.celebrationType} onChange={this.celebrationTypeChanged}>
            <div id="typeOfCelebration">
              <label htmlFor="typeOfCelebration" className="heading">Celebration type:</label>
              <label>
                <Radio value="birthday" />Birthday
              </label>
              <label>
                <Radio value="wedding" />Wedding
              </label>
              <label>
                <Radio value="corporate" />Corporate
              </label>
              <label>
                <Radio value="other" />Other
              </label>
              <input type="text" id="other" value={this.state.otherCelebrationType} onChange={this.otherCelebrationTypeChanged} disabled={this.state.celebrationType!='other'} />
            </div>
          </RadioGroup>

          <label htmlFor="about" className="heading">Tell us about your dream cake:</label>
          <textarea rows="3" name="Text" id="Text" value={this.state.about} onChange={this.aboutChanged} />

          <div id="errorMessage">{this.state.errorMessage}</div>
          <button id="btnSubmit" onClick={this.validate}>Send Away</button>
          <button id="btnReset" onClick={this.reset}>Reset</button>
        </form>
      </div>
    );
  }

  nameChanged(event){
    this.setState({
      name: event.target.value
    });
  }

  emailChanged(event){
    this.setState({
      email: event.target.value
    });
  }

  cakeTypesChanged(newCakes){
    this.setState({
      cakeTypes: newCakes
    });
  }

  celebrationTypeChanged(newCelebrationType){
    if (newCelebrationType != 'other'){
      this.setState({
        otherCelebrationType: ''
      });
    }
    this.setState({
      celebrationType: newCelebrationType
    });
  }

  otherCelebrationTypeChanged(event){
    this.setState({
      otherCelebrationType: event.target.value
    });
  }

  aboutChanged(event){
    this.setState({
      about: event.target.value
    });
  }

  validate(event){
    event.preventDefault();
    var formValid = true;
    var formMessage = '';
    if(this.state.name.length == 0){
      formMessage = 'Please enter a name';
      formValid = false;
    }else if(this.state.email.length == 0){
      formMessage = 'Please enter a email';
      formValid = false;
    }else if(!this.validateEmail(this.state.email)){
      formMessage = 'Please enter a valid email';
      formValid = false;
    }else if(this.state.celebrationType.length == 0){
      formMessage = 'Please enter a celebration type';
      formValid = false;
    }else if(this.state.celebrationType === 'other'){
      if(this.state.otherCelebrationType.length == 0){
        formMessage = 'Please enter a other celebration type';
        formValid = false;
      }
    }

    this.setState({
      errorMessage: formMessage
    });

    if(formValid){
      alert('form is valid');
    }
  }

  validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  reset(event){
    event.preventDefault()
    this.setState({
      name: '',
      email: '',
      cakeTypes: [],
      celebrationType: '',
      otherCelebrationType: '',
      about: '',
      errorMessage: '',
      formValid: false
    });
  }
}

export default CakeForm;
